[![Hex.pm](https://img.shields.io/hexpm/l/plug.svg)]()
[![Build Status](https://travis-ci.org/grzesuav/gjpf-core.svg?branch=develop)](https://travis-ci.org/grzesuav/gjpf-core) 
[![Coverage Status](https://coveralls.io/repos/grzesuav/gjpf-core/badge.svg)](https://coveralls.io/r/grzesuav/gjpf-core) 
[![HuBoard badge](http://img.shields.io/badge/Hu-Board-7965cc.svg)](https://huboard.com/grzesuav/gjpf-core)


This is fork of Java Path Finder project (which is one-to-one mirrored [here](https://github.com/grzesuav/jpf-core)).


Main reasons/motivation:
* desire to contribute in some OS project
* experiment with new features
* maybe contribute some changes to original JPF.

Currently:
* setup of Travis CI build was configured
* develop branch was created
* coverity-scan branch was created and coverity static analysis scan was attached (see branch coverity-scan to see results)

[![Join the chat at https://gitter.im/grzesuav/gjpf-core](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/grzesuav/gjpf-core?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)